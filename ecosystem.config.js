module.exports = {
	apps: [
		{
			name: 'OTON CONTÁBIL CLIENT',
			script: 'scripts/start-production.js',
			watch: 'true',
			env: {
				NODE_ENV: 'development',
				IP: '187.49.247.78',
				PORT: '80'
			},
			env_production: {
				NODE_ENV: 'production',
				IP: '187.49.247.78',
				PORT: '80'
			},
			env_test: {
				NODE_ENV: 'test',
				IP: '187.49.247.78',
				PORT: '80'
			}
		}
	],
	deploy: {
		production: {
			user: 'root',
			host: [
				{
					host: '187.49.247.78',
					port: '9022'
				}
			],
			ref: 'origin/develop',
			repo: 'git@bitbucket.org:oton_solut/oton_client_base.git',
			path: '/home/client.contabil',
			'post-deploy': 'npm install && npm run build && pm2 reload ecosystem.config.js --env production'
		},
		dev: {
			user: 'root',
			host: [
				{
					host: '187.49.247.78',
					port: '9022'
				}
			],
			ref: 'origin/develop',
			repo: 'git@bitbucket.org:oton_solut/oton_contabil_api.git',
			path: '/home/api.contabil',
			'post-deploy': 'npm install && npm run build && pm2 reload ecosystem.config.js --env development',
			env: {
				NODE_ENV: 'dev'
			}
		}
	}
};
