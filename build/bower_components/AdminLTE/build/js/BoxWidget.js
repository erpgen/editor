/* BoxWidget()
* ======
* Adds box widget functions to boxes.
*
* @Usage: $('.my-box').boxWidget(options)
*         This plugin auto activates on any element using the `.box` class
*         Pass any option as data-option="value"
*/
+function ($) {
	'use strict'

	let DataKey = 'lte.boxwidget'
	console.log("AAAAAAAAAAAAa")

	let Default = {
		animationSpeed : 500,
		collapseTrigger: '[data-widget="collapse"]',
		removeTrigger  : '[data-widget="remove"]',
		collapseIcon   : 'fa-minus',
		expandIcon     : 'fa-plus',
		removeIcon     : 'fa-times'
	}

	let Selector = {
		data     : '.box',
		collapsed: '.collapsed-box',
		body     : '.box-body',
		footer   : '.box-footer',
		tools    : '.box-tools'
	}

	let ClassName = {
		collapsed: 'collapsed-box'
	}

	let Event = {
		collapsed: 'collapsed.boxwidget',
		expanded : 'expanded.boxwidget',
		removed  : 'removed.boxwidget'
	}

	// BoxWidget Class Definition
	// =====================
	let BoxWidget = function (element, options) {
		this.element = element
		this.options = options

		this._setUpListeners()
	}

	BoxWidget.prototype.toggle = function () {
		let isOpen = !$(this.element).is(Selector.collapsed)

		if (isOpen) {
			this.collapse()
		} else {
			this.expand()
		}
	}

	BoxWidget.prototype.expand = function () {
		let expandedEvent = $.Event(Event.expanded)
		let collapseIcon  = this.options.collapseIcon
		let expandIcon    = this.options.expandIcon

		$(this.element).removeClass(ClassName.collapsed)

		$(this.element)
		.find(Selector.tools)
		.find('.' + expandIcon)
		.removeClass(expandIcon)
		.addClass(collapseIcon)

		$(this.element).find(Selector.body + ', ' + Selector.footer)
		.slideDown(this.options.animationSpeed, function () {
			$(this.element).trigger(expandedEvent)
		}.bind(this))
	}

	BoxWidget.prototype.collapse = function () {
		let collapsedEvent = $.Event(Event.collapsed)
		let collapseIcon   = this.options.collapseIcon
		let expandIcon     = this.options.expandIcon

		$(this.element)
		.find(Selector.tools)
		.find('.' + collapseIcon)
		.removeClass(collapseIcon)
		.addClass(expandIcon)

		$(this.element).find(Selector.body + ', ' + Selector.footer)
		.slideUp(this.options.animationSpeed, function () {
			$(this.element).addClass(ClassName.collapsed)
			$(this.element).trigger(collapsedEvent)
		}.bind(this))
	}

	BoxWidget.prototype.remove = function () {
		let removedEvent = $.Event(Event.removed)

		$(this.element).slideUp(this.options.animationSpeed, function () {
			$(this.element).trigger(removedEvent)
			$(this.element).hide()
		}.bind(this))
	}

	// Private

	BoxWidget.prototype._setUpListeners = function () {
		let that = this

		$(this.element).on('click', this.options.collapseTrigger, function (event) {
			if (event) event.preventDefault()
			that.toggle()
		})

		$(this.element).on('click', this.options.removeTrigger, function (event) {
			if (event) event.preventDefault()
			that.remove()
		})
	}

	// Plugin Definition
	// =================
	function Plugin(option) {
		return this.each(function () {
			let $this = $(this)
			let data  = $this.data(DataKey)

			if (!data) {
				let options = $.extend({}, Default, $this.data(), typeof option == 'object' && option)
				$this.data(DataKey, (data = new BoxWidget($this, options)))
			}

			if (typeof option == 'string') {
				if (typeof data[option] == 'undefined') {
					throw new Error('No method named ' + option)
				}
				data[option]()
			}
		})
	}

	let old = $.fn.boxWidget

	$.fn.boxWidget             = Plugin
	$.fn.boxWidget.Constructor = BoxWidget

	// No Conflict Mode
	// ================
	$.fn.boxWidget.noConflict = function () {
		$.fn.boxWidget = old
		return this
	}

	// BoxWidget Data API
	// ==================
	$(window).on('load', function () {
		$(Selector.data).each(function () {
			Plugin.call($(this))
		})
	})

}(jQuery)
