'use strict';

const path  = require('path');
const express = require('express');
const getClientEnvironment = require('../config/env');
const compression = require('compression');

// Lê o .env e mais algumas váriaveis de enviroment
process.env = getClientEnvironment('../').raw;

const app = express();
const DIST_DIR  = path.join(__dirname, "../build");
const HTML_FILE = path.join(DIST_DIR, "index.html");

app.use(compression());
app.use(express.static(DIST_DIR));

app.get("*", (req, res) => res.sendFile(HTML_FILE));

//app.get('/:file', (req, res) => res.sendFile(path.join(DIST_DIR, req.params.file)));

app.listen(process.env.REACT_APP_PORT, process.env.REACT_APP_HOST, () => {
	console.log(`React App listening on ${process.env.REACT_APP_HOST}:${process.env.REACT_APP_PORT}`);
})
