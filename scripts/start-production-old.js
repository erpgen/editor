'use strict';

const path  = require('path');
const express = require('express');
const farmhash = require('farmhash');
const net = require('net');
const httpServer = require('http');
const numCPUs = require('os').cpus().length;
const cluster = require('cluster');
const getClientEnvironment = require('../config/env');

// Lê o .env e mais algumas váriaveis de enviroment
process.env = getClientEnvironment('../').raw;

// BASE
const app = express();
app.cluster = cluster;
const SERVER = httpServer.createServer(app);
const DIST_DIR  = path.join(__dirname, "../build");
const HTML_FILE = path.join(DIST_DIR, "index.html");

app.use(express.static(DIST_DIR));

app.get("*", (req, res) => res.sendFile(HTML_FILE));

// /BASE

// CLUSTER
if (app.cluster.isMaster) {
	console.log('Master thread sends Hello.');

	const workers = [];

	const spawn = function(i) {
		workers[i] = app.cluster.fork();

		workers[i].on('exit', () => { // (code, signal) =>
			console.log('respawning worker', i);
			spawn(i);
		});
	};

	for (let i = 0; i < numCPUs; i++) {
		spawn(i);
	}

	const workerIndex = function(ip, len) {
		return farmhash.fingerprint32(ip) % len; // ERA ip[i] Farmhash is the fastest and works with IPv6, too
	};

	net.createServer({ pauseOnConnect: true }, (connection) => {
		const worker = workers[workerIndex(connection.remoteAddress, numCPUs)];
		worker.send('sticky-session:connection', connection);
	}).listen(process.env.REACT_APP_PORT);
} else {
	const server = SERVER.listen(0, process.env.REACT_APP_HOST, () => {
		console.log(`Worker thread ${app.cluster.worker.id} is listening on 0 in host ${process.env.REACT_APP_HOST}.`);
	});

  // Listen to messages sent from the master. Ignore everything else.
	process.on('message', (message, connection) => {
		if (message !== 'sticky-session:connection') {
			return;
		}
		console.log(`Worker Thread ${app.cluster.worker.id} is handling this request.`);

		server.emit('connection', connection);

		connection.resume();
	});
}
