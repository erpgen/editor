import { take, fork, call, put, select, all } from 'redux-saga/effects';
import axios from 'axios';
import { setSubmitFailed } from 'redux-form';
//import { userData } from '../selectors';
import store from '../store';

import { LOGIN_REQUESTING, LOGIN_SUCCESS, LOGIN_ERROR, FETCH_TO_SELECT } from '../constants/login';
import clientesAction from '../actions/client';
import history from '../util/history';
// import ThrowError from '../util/throwError';
import Toast from '../components/Toast';

/*function* getHeaders() {
	const headers = yield select(userData);
	return {
		headers: {
			Authorization: `JWT ${headers.token}`,
			'x-oton-userid': headers.id,
			'x-oton-username': headers.nome,
			'x-oton-userlogin': headers.login,
			'x-oton-cliente-id': headers.empresa,
			'Content-Encoding': 'gzip'
		}
	};
}
*/
const loginUrl = `${process.env.REACT_APP_API_URL}/login`;

/* ============ API ============ */
function loginApi(email, senha, io) {
	return axios
	.post(loginUrl, { login: email, senha })
	.then((response) => {
		io.emit('drop-client', { id: response.data.userId, msg: 'Você foi desconectado devido a tentativa de login simultaneo em outro navegador' });
		return response;
	})
	.catch(error => error.response.status);
}


/* ============ WORKERS ============ */

function* loginFlow(email, senha) {
	let token;

	try {
		token = yield call(loginApi, email, senha);

		if (token >= 400) {
			new Toast().error('Confira os dados e tente novamente!');
			store.dispatch(setSubmitFailed('login'));
		}

		axios.defaults.headers.common.Authorization = `JWT ${token.data.token}`;
		axios.defaults.headers.common['x-oton-userid'] = token.data.userId;
		axios.defaults.headers.common['x-oton-userlogin'] = token.data.login;

		// socket.emit('response-user', { email: token.data.login, id: token.data.userId, empresa: token.data.portal_clientes_fk, operador: token.data.nome });
		// socket.emit('fetch_notifications', { id: token.data.userId, empresa: token.data.portal_clientes_fk });

		// inform Redux to set our client token, this is non blocking so...
		yield put(clientesAction.setClient(token.data));

		// .. also inform redux that our login was successful
		yield put({ type: LOGIN_SUCCESS });

		// set a stringified version of our token to localstorage on our domain
		localStorage.setItem('token', JSON.stringify(token));
		// redirect them to Dashboard!
		//	yield put(clientesAction.fetchName({ userId: token.data.userId, localId: token.data.localId, pessoaId: token.data.pessoaId }));
		yield put({ type: FETCH_TO_SELECT });
		history.replace('/dashboard');
	} catch (error) {
		yield put({ type: LOGIN_ERROR, error });
	}
}


/* ============ WATCHERS ============ */

function* loginWatcher() {
	while (true) {
		const { email, senha } = yield take(LOGIN_REQUESTING);
		yield fork(loginFlow, email, senha);
	}
}

export default function* LoginSagas() {
	yield all([
		fork(loginWatcher)
	]);
}
