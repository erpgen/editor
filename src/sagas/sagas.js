import LoginSagas from './sagas-login';

export default function* root() {
	yield [
		LoginSagas()
	];
}
