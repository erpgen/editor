import * as CLIENTE from '../constants/client';

const clientesAction = {
	setClient: token => ({
		type: CLIENTE.CLIENT_SET, token
	}),
	// unsetClient: () => ({ type: CLIENTE.CLIENT_UNSET }),
	fetchName: data => ({ type: CLIENTE.FETCH_NAME, data }),
	setName: nome => ({ type: CLIENTE.SET_NAME, nome })
};

export default clientesAction;
