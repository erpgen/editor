export const CLIENT_SET = 'CLIENT_SET';
export const CLIENT_UNSET = 'CLIENT_UNSET';
export const FETCH_NAME = 'FETCH_NAME';
export const SET_NAME = 'SET_NAME';
export const SET_SOCKET = 'SET_SOCKET';
export const PING = 'PING';
