import { createSelector } from 'reselect';

const filtraValores = (arr) => {
	const valor = arr;
	try {
		for (let i = 0; i < valor.length; i++) {
			const value = (Object.prototype.toString.call(valor[i][Object.keys(valor[i])[0]]) === '[object Array]') ? Object.keys(valor[i])[1] : Object.keys(valor[i])[0];
			const array = (Object.prototype.toString.call(valor[i][Object.keys(valor[i])[0]]) === '[object Array]') ? Object.keys(valor[i])[0] : Object.keys(valor[i])[1];

			Object.defineProperty(valor[i], 'value', Object.getOwnPropertyDescriptor(valor[i], value));
			Object.defineProperty(valor[i], 'label', Object.getOwnPropertyDescriptor(valor[i], value));
			delete valor[i][value];
			delete valor[i][array];
		}
	} catch (e) {
		throw new Error(e);
	}
	return valor;
};

const localEstado = state => state.local.data.estado;

export const getEstado = createSelector([localEstado], () => { filtraValores(localEstado); });

export const userData = state => state.client;
