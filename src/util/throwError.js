export default function ThrowError(error) {
	if (error.response) {
		console.log(error.response);
		if (typeof error.response.data.meta.message === 'object') {
			console.log(error.response.data.meta);
			throw new Error(error.response.data.meta.message.message);
		} else {
			throw new Error(error.response.data.meta.message);
		}
	} else if (error.request) {
		throw new Error('Problemas de Comunicação com o Servidor');
	} else {
		throw new Error('Problema!!');
	}
}
