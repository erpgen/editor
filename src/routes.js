import React from 'react';
import { Route, Switch } from 'react-router';
import NotFoundPage from './containers/NotFoundPage.js';
import FormPage from './containers/FormPage';
import TablePage from './containers/TablePage';
import Dashboard from './containers/DashboardPage';

export default (
  <Switch>
      <Route path="/" exact component={Dashboard}/>
      <Route path="/dashboard" component={Dashboard}/>
      <Route path="/form" component={FormPage}/>
      <Route path="/table" component={TablePage}/>
      <Route path="*" component={NotFoundPage}/>
  </Switch>
);
