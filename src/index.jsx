/* eslint-disable import/default */

import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { render } from 'react-dom';
import { Router, Switch, Route } from 'react-router';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import store from './store';
import routes from './routes';
import App from './containers/App';
import LoginPage from './containers/LoginPage';
import './styles.css';
import 'font-awesome/css/font-awesome.css';
import 'flexboxgrid/css/flexboxgrid.css';
require('./favicon.ico');

export default class AppProvider extends Component {
	constructor() {
		super();
		this.state = { rehydrated: false };
	}

	componentWillMount() {
		persistStore(store, {}, () => {
			this.setState({ rehydrated: true });
		});
	}

	render() {
		const history = createHistory();

		if (!this.state.rehydrated) { return (<div />); }

		return (
			<div>
				<Provider store={store}>
					<Router history={history}>
						<Switch>
							<Route path="/login" exact component={LoginPage} />
							<Route history={history} path="/" component={App} />
						</Switch>
					</Router>

				</Provider>
			</div>
		);
	}
}

render(
	<AppProvider />,
	document.getElementById('app')
);
