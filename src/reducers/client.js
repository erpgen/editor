import * as CLIENTE from '../constants/client';
import { LOGOUT } from '../constants/login';

const initialState = {
	id: null,
	token: null,
	socket: null,
	nome: null,
	login: null,
	id_local: null,
	id_pessoa: null,
	tokenExpires: null,
	app_version: null
};

const clientReducer = function clientReducer(state = initialState, action) {
	switch (action.type) {
		case CLIENTE.CLIENT_SET:
			return {
				id: action.token.userId,
				token: action.token.token,
				nome: action.token.nome,
				login: action.token.login,
				id_local: action.token.localId,
				id_pessoa: action.token.pessoaId,
				empresa: action.token.portal_clientes_fk,
				app_version: action.token.app_version,
				tokenExpires: action.token.tokenExpires
			};

		case LOGOUT:
			return initialState;

		case CLIENTE.CLIENT_UNSET:
			return {
				id: null,
				token: null,
				nome: null,
				login: null
			};

		case CLIENTE.SET_SOCKET:
			return {
				...state,
				socket: action.socket
			};

		case CLIENTE.SET_NAME:
			return {
				...state,
				nome: action.nome
			};

		default:
			return state;
	}
};

export default clientReducer;
