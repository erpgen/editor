import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';

import loginReducer from './login';
import clientReducer from './client';
import architectureReducer from './architecture';

const rootReducer = combineReducers({
	architecture: architectureReducer,
	client: clientReducer,
	login: loginReducer,
	form: formReducer,
	routing: routerReducer
});

export default rootReducer;
