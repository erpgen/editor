import { LOGIN_SUCCESS, LOGIN_ERROR } from '../constants/login';

const initialState = {
	requesting: false, successful: false, messages: [], errors: []
};

const loginReducer = function loginReducer(state = initialState, action) {
	switch (action.type) {
		case LOGIN_SUCCESS:
			return {
				errors: [],
				messages: [],
				requesting: false,
				successful: true
			};

		case LOGIN_ERROR:
			return {
				errors: state.errors.concat([{
					body: action.error.toString(),
					time: new Date()
				}]),
				messages: [],
				requesting: false,
				successful: false
			};

		default:
			return state;
	}
};

export default loginReducer;
