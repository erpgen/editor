import { SET_ARCHITECTURE, SET_MENU } from '../constants/architecture';

const initialState = {
	modules: [],
	tables: [],
	menus: [
		{ text: 'DashBoard', icon: 'assessment', link: '/dashboard' },
		{ text: 'Form Page', icon: 'web', link: '/form' },
		{ text: 'Table Page', icon: 'gridOn', link: '/table' },
		{ text: 'Login Page', icon: 'permIdentity', link: '/login' }
	]
};

const architectureReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_ARCHITECTURE:
			return action.architecture;

		case SET_MENU:
			return {
				...state,
				menus: [...state.menus, action.menu]
			};

		default:
			return state;
	}
};

export default architectureReducer;
