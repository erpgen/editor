import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { autoRehydrate, persistStore } from 'redux-persist';
import { routerMiddleware } from 'react-router-redux';
import history from './util/history';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootSagas from './sagas/sagas';

import rootReducer from './reducers/index';

const defaultState = {};
const sagaMiddleware = createSagaMiddleware();
const router = routerMiddleware(history);
const composeEnhancers = composeWithDevTools({
	actionsBlacklist: ['@@redux'],
	maxAge: 150
});
const store = createStore(rootReducer, defaultState, composeEnhancers(autoRehydrate(), applyMiddleware(sagaMiddleware, router)));

const persistor = persistStore(store);

sagaMiddleware.run(rootSagas);

export default store;
export { persistor };
