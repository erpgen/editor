import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { isValidArray } from '../util/isValidVariable';
import store from '../store';

let toastId;
class Toast extends Component {
	constructor(props) {
		super(props);
		this.toast = toast;

		this.info = this.info.bind(this);
		this.success = this.success.bind(this);
		this.warn = this.warn.bind(this);
		this.error = this.error.bind(this);
	}

	componentDidMount() {
		if (isValidArray(store.getState().toast.toast)) {
			store.getState().toast.toast.forEach((value, index) => {
				toast.dismiss(value.toastId);
				store.dispatch({ type: 'DELETE_TOAST', toastId: value.toastId, index });
			});
		}
	}

	componentWillReceiveProps(nextProps) {
		if (isValidArray(store.getState().toast.toast)) {
			store.getState().toast.toast.forEach((value, index) => {
				if (value.url !== nextProps.routing.location.pathname) {
					toast.dismiss(value.toastId);
					store.dispatch({ type: 'DELETE_TOAST', toastId: value.toastId, index });
				}
			});
		}
	}

	dismiss(id) {
		this.toast.dismiss(id);
	}

	info(value) {
		const indice = store.getState().toast.toast.findIndex(element => (element.text === value));
		if (indice === -1) {
			toastId = this.toast.info(<div><span className="fa fa-info-circle" style={{ fontSize: '14px', display: 'inline' }} />&nbsp;&nbsp;<div style={{ fontSize: '13px', display: 'inline' }}>{value}</div></div>, { autoClose: 2000,
				onClose: () => {
					const index = store.getState().toast.toast.findIndex(element => element.toastId === toastId);
					store.dispatch({ type: 'DELETE_TOAST', toastId, index });
				} });
			store.dispatch({ type: 'ADD_TOAST', toastId, text: value, tipo: 'warn', url: store.getState().routing.location.pathname });
		}
	}

	success(value) {
		const indice = store.getState().toast.toast.findIndex(element => (element.text === value));
		if (indice === -1) {
			toastId = this.toast.success(<div><span className="fa fa-check-circle" style={{ fontSize: '14px', display: 'inline' }} />&nbsp;&nbsp;<div style={{ fontSize: '13px', display: 'inline' }}>{value}</div></div>, { autoClose: 2000,
				onClose: () => {
					const index = store.getState().toast.toast.findIndex(element => element.toastId === toastId);
					store.dispatch({ type: 'DELETE_TOAST', toastId, index });
				} });
			store.dispatch({ type: 'ADD_TOAST', toastId, text: value, tipo: 'warn', url: store.getState().routing.location.pathname });
		}
	}

	warn(value) {
		const indice = store.getState().toast.toast.findIndex(element => (element.text === value));
		if (indice === -1) {
			toastId = this.toast.warn(<div><span className="fa fa-warning" style={{ fontSize: '14px', display: 'inline' }} />&nbsp;&nbsp;<div style={{ fontSize: '13px', display: 'inline' }}>{value}</div></div>, { autoClose: false,
				onClose: () => {
					const index = store.getState().toast.toast.findIndex(element => element.toastId === toastId);
					store.dispatch({ type: 'DELETE_TOAST', toastId, index });
				} });
			store.dispatch({ type: 'ADD_TOAST', toastId, text: value, tipo: 'warn', url: store.getState().routing.location.pathname });
		}
	}

	error(value) {
		const indice = store.getState().toast.toast.findIndex(element => (element.text === value));
		if (indice === -1) {
			toastId = this.toast.error(<div><span className="fa fa-times-circle" style={{ fontSize: '14px', display: 'inline' }} />&nbsp;&nbsp;<div style={{ fontSize: '13px', display: 'inline' }}>{value}</div></div>, { autoClose: false,
				onClose: () => {
					const index = store.getState().toast.toast.findIndex(element => element.toastId === toastId);
					store.dispatch({ type: 'DELETE_TOAST', toastId, index });
				} });
			store.dispatch({ type: 'ADD_TOAST', toastId, text: value, tipo: 'error', url: store.getState().routing.location.pathname });
		}
	}

	render() {
		return (
			<ToastContainer
				position={(typeof this.props.position !== 'undefined') ? this.props.position : 'top-right'}
				type="default"
				autoClose={(typeof this.props.autoClose !== 'undefined') ? this.props.autoClose : 2000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				style={{ zIndex: '99999999999999999' }}
				pauseOnHover
			/>
		);
	}
}

Toast.propTypes = { routing: PropTypes.shape({ location: PropTypes.shape({ pathname: PropTypes.string }) }),
	autoClose: PropTypes.number,
	position: PropTypes.string };

Toast.defaultProps = { autoClose: 2000,
	position: 'top-right',
	routing: { location: { pathname: '' } } };

export default Toast;
