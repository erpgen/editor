import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Menu from 'material-ui/svg-icons/navigation/menu';
import ViewModule from 'material-ui/svg-icons/action/view-module';
import {blue600} from 'material-ui/styles/colors';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class EditorHeader extends React.Component {

  render() {
    const {styles} = this.props;

    const style = {
      appBar: {
        position: 'relative',
        top: 0,
        overflow: 'hidden',
        maxHeight: 57,
				backgroundColor: '#fff',
				color: '#000'
      },
      menuButton: {
        marginLeft: 10
      },
      iconsRightContainer: {
        marginLeft: 20
      }
    };

    return (
        <div>
            <AppBar
              style={{...styles, ...style.appBar}}
							title={<span style={{color: '#000'}}>ERPGEN</span>}

							iconElementRight={
                <div style={style.iconsRightContainer}>
                  <IconMenu color={blue600}
                            iconButtonElement={
                              <IconButton><ViewModule color={blue600}/></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                  >
                    <MenuItem key={1} primaryText="Application 1"/>
                    <MenuItem key={2} primaryText="Application 2"/>
                    <MenuItem key={3} primaryText="Application 3"/>
                  </IconMenu>
                  <IconMenu color={blue600}
                            iconButtonElement={
                              <IconButton><MoreVertIcon color={blue600}/></IconButton>
                            }
                            targetOrigin={{horizontal: 'right', vertical: 'top'}}
                            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                  >
                    <MenuItem primaryText="Sign out" containerElement={<Link to="/login"/>}/>
                  </IconMenu>
                </div>
              }
            />
          </div>
      );
  }
}

EditorHeader.propTypes = {
  styles: PropTypes.object,
  handleChangeRequestNavDrawer: PropTypes.func
};

export default EditorHeader;
