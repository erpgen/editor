import React, { Component } from 'react';
import { OffCanvas, OffCanvasMenu, OffCanvasBody } from 'react-offcanvas';
import Paper from 'material-ui/Paper';
import globalStyles from '../../styles';
import {blue600, grey900} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
	editorMenuContainer: {
		padding: '10px',
		height: '400px'
	}
}

class EditorSidebar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isMenuOpened: true
		};
	}

	componentWillMount() {
		// sets the initial state
		this.setState({
			isMenuOpened: true
		});
	}

	handleClick() {
		this.setState({
			isMenuOpened: !this.state.isMenuOpened
		});
	}

	render() {
		return (
			<OffCanvas width={200} transitionDuration={300} isMenuOpened={this.state.isMenuOpened} position={"right"}>
				<OffCanvasBody style={{fontSize: '30px'}}>
					<div style={{ width: '100px', marginTop: '300px', height: '100px', right: '0', position: 'absolute', backgroundColor: '#ffff', boxShadow: '1px 1px 10px 1px' }}>
						<p><a href="#" onClick={this.handleClick.bind(this)}>toggle</a></p>
					</div>
				</OffCanvasBody>
				<OffCanvasMenu style={{ zIndex: 9999999999}}>
					<Paper style={{ ...globalStyles.paper, marginTop: '100px', padding: 0, zIndex: '99999999999'  }}>
						<div style={{ backgroundColor: blue600 }}>
							<h2 style={{ color: '#fff', textAlign: 'center', padding: '5px' }}>Editor</h2>
						</div>
						<div style={styles.editorMenuContainer}>
							<RaisedButton label="Novo" style={{ width: '100%' }} type="button" primary={true}/>
						</div>
					</Paper>
				</OffCanvasMenu>
			</OffCanvas>
		);
	}
}

export default EditorSidebar;
