import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EditorSidebar from './EditorSidebar';
import EditorHeader from './EditorHeader';

class Editor extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<EditorHeader />
				<EditorSidebar />
				{ this.props.children }
			</div>
		);
	}
}

Editor.propTypes = {
	children: PropTypes.element
};

export default Editor;
