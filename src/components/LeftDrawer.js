import React,  { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from 'material-ui/Drawer';
import {spacing, typography} from 'material-ui/styles';
import {white, blue600} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';
import Assessment from 'material-ui/svg-icons/action/assessment';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Web from 'material-ui/svg-icons/av/web';
import store from '../store';

class LeftDrawer extends Component {
	constructor(props) {
		super(props);

		this.props = props;
		this.addMenu = this.addMenu.bind(this);
	}

	addMenu() {
		store.dispatch({ type: 'SET_MENU', menu: { text: 'teste', icon: 'web', link: '/teste'} });
	}

	render() {

		let { navDrawerOpen } = this.props;

		const icons = {
			assessment: <Assessment/>,
			web: <Web/>,
			gridOn: <GridOn/>,
			permIdentity: <PermIdentity/>
		};

		const styles = {
			logo: {
				cursor: 'pointer',
				fontSize: 22,
				color: typography.textFullWhite,
				lineHeight: `${spacing.desktopKeylineIncrement}px`,
				fontWeight: typography.fontWeightLight,
				backgroundColor: blue600,
				paddingLeft: 40,
				height: 56,
			},
			menuItem: {
				color: white,
				fontSize: 14
			},
			avatar: {
				div: {
					padding: '15px 0 20px 15px',
					backgroundImage:  'url(' + require('../images/material_bg.png') + ')',
					height: 45
				},
				icon: {
					float: 'left',
					display: 'block',
					marginRight: 15,
					boxShadow: '0px 0px 0px 8px rgba(0,0,0,0.2)'
				},
				span: {
					paddingTop: 12,
					display: 'block',
					color: 'white',
					fontWeight: 300,
					textShadow: '1px 1px #444'
				}
			}
		};

		return (
			<Drawer docked={true} open={navDrawerOpen}>
				<div style={styles.logo}>
					Material Admin
				</div>
				<div style={styles.avatar.div}>
					<Avatar src="http://www.material-ui.com/images/uxceo-128.jpg"
					size={50}
					style={styles.avatar.icon}
				/>
				</div>
				<span style={styles.avatar.span}>{this.props.username}</span>
				<div>
					{this.props.menus.map((menu, index) =>
						<MenuItem
							key={index}
							style={styles.menuItem}
							primaryText={menu.text}
							leftIcon={icons[menu.icon]}
							containerElement={<Link to={menu.link}/>}
						/>
					)}
					<button onClick={this.addMenu} style={styles.menuItem}>Adicionar Menu</button>
				</div>
			</Drawer>
		);
	}
}

LeftDrawer.propTypes = {
	navDrawerOpen: PropTypes.bool,
	menus: PropTypes.array,
	username: PropTypes.string,
};


const mapStateToProps = state => ({
	menus: state.architecture.menus
});

export default connect(mapStateToProps)(LeftDrawer);
